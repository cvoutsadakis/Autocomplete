package ce325.hw1;

import java.io.*;
import static java.lang.Character.isLowerCase;
import java.util.*;
import java.util.regex.*;
import static jdk.nashorn.tools.ShellFunctions.input;


public class MainClass implements java.io.Serializable{
    
    
    public static void traverse(DictTree tree, DictNode root, List<String> suggestions){ // post order traversal
        System.out.println(root.children[0].data);
        DictNode currentNode = root;
        String letters;
        letters = "";
        Set<String> words = null;
        
        //suggestions = traverseRecursive(tree, currentNode, suggestions, letters);
        suggestions = recursiveSuggest(root, letters, suggestions);
        System.out.println(suggestions);
        
        
    }
    
    
    
    public static List<String> traverseRecursive(DictTree tree, DictNode currentNode, List<String> suggestions, String letters) {
        DictNode nextNode;
        String word = null;
        
        for(int i = 0; i < 26; i++){
           
                
                
            
            
            if(currentNode.children[i] == null) {

                continue;
            }
            else {
                nextNode = currentNode.children[i];
                letters = letters + currentNode.data;
                System.out.println("Printing letters so far: " +letters);
                if(nextNode.isWordEnd) {
                    word = letters;
                    System.out.println("Current node data: "+nextNode.data +" letters: " +letters);
                    System.out.println("The word is: "+word);
                    suggestions.add(word);
                    //letters = null;
                }
                suggestions = traverseRecursive(tree, nextNode, suggestions, letters);
            }

        }

        return suggestions;
            
            
    }
    

    
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      
    public static DictTree deserialize(String FromFilePath) throws FileNotFoundException, UnsupportedEncodingException, ClassNotFoundException {
        
        DictTree tree = new DictTree() ;
        

        try {
            FileInputStream loadedTree = new FileInputStream(FromFilePath);
            ObjectInputStream in = new ObjectInputStream(loadedTree);
            
            tree = (DictTree) in.readObject();
            in.close();
            loadedTree.close();
            System.out.println(tree.size);
            
            
            
                   
        } catch(IOException ex) {
            ex.printStackTrace();
            //return;
        }
        return tree;
    }
    
    
    
    
    public static void serialize(DictTree tree, String FilePath) {
            
           
        try (FileOutputStream serializedDict = new FileOutputStream(FilePath);){         
            ObjectOutputStream out = new ObjectOutputStream(serializedDict);
            out.writeObject(tree);
            
            out.close();
            serializedDict.close();
            System.out.println("Tree size: "+ tree.size);
         } catch(IOException ex) {
            ex.printStackTrace();
            
         }
      
    }
    
   
    
    
    public static void createTree(String word, DictNode root, DictTree Tree) {
        int n;
        n = 0;
        
        
        DictNode newLeaf = null;
        
        DictNode currentNode = root;
        
		
		
        for(int i = 0; i < word.length(); i++) {
            char currentChar = word.charAt(i);
            if (isLowerCase(currentChar)) {
                n = (int) currentChar - (int) 'a';
            }
            else{
                n = (int) currentChar - (int) 'A';
            }
        		
            // each for loop that isn't for the first word, has the following check, to see if the leaf (letter/prefix/word) already exists
            if(currentNode.children[n] == null) {
               
                newLeaf = Tree.addLeaf(currentChar, currentNode, n);
            
                
                currentNode = newLeaf;
            }
            else {
             
                currentNode = currentNode.children[n];
                
            }
            
            if(i==word.length() - 1) {
             
                currentNode.isWordEnd = true;
            }               
        }  
    }
    


    
    public  static boolean isAlpha(String  word) {
        return word.matches("[a-zA-Z]+");
    }

    
    
    
    
    
    
    public static void readFiles(String filePath, DictNode root, DictTree tree, Set set) throws FileNotFoundException {
	try {
            File dir = new File(filePath);
        
            //PrintWriter writer = new PrintWriter("output.txt", "UTF-8"); // TO OUTPUT MPAINEI SE ARXEIO TXT
            if(dir.isDirectory()) {

                for(File file : dir.listFiles()) {
                    if(file.isFile()) {

                        Scanner sc = new Scanner(file); //open and read file from path

                        // thisPunct is true when the current word ends with Punctuation
                        // afterPunct is true when the current word follows a word ending with Punctuation
                        boolean thisPunct = false;
                        boolean afterPunct = false;
                        
			while( sc.hasNext() ) { //while not end of file is reached
                            String word = sc.next();  //read word by word

                        
                            if(thisPunct) { 
                                afterPunct = true;
                            }

                            thisPunct = false;

                            Pattern midpunct = Pattern.compile("\\p{Alpha}\\p{Punct}\\p{Alpha}"); //find patterns in words with mid punctuation
                            Matcher midmatcher = midpunct.matcher(word);

                            if(midmatcher.find()) { //if there is a word with mid punctuation continue without saving it
                            
                                continue;
                            }

                            Pattern p = Pattern.compile("\\p{Punct}"); //find words with punctuation in the end
                            Matcher m = p.matcher(word);

                            if(m.find()) { 
                               
                                // if a word starts with punctuation
                                if(m.start() == 0) {
                                      
                                    word = word.substring(1,word.length());
                                 
                                }
                               
                                //if a word with punctuation in the end is found save the word without the punctuation
                                if(m.start() == (word.length()-1)) {
                                    word = word.substring(0,m.start());
                                }
                                
                                thisPunct = true;
                                //System.out.println("Word WITHOUT PUNC "+ word);
                            }

                            //Trimming the spaces
                            word = word.trim();


                            // if a word that follows a word with punctuation in the end is not fully in caps or fully in small letters, print the small letter version
                            if (afterPunct) {
                                if((word != word.toLowerCase()) && (word != word.toUpperCase())){
                                    word = word.toLowerCase();

                                }
                                afterPunct = false;
                            }

                            if(!isAlpha(word)) {
                                    continue;
                            }
                            if (set.add(word)) {
                                createTree(word, root, tree);
                            }
                          
                        
			} 
                        sc.close();
                    }
                }
            }
            
            else {
                

                Scanner sc = new Scanner(new File(filePath)); //open and read file from path


                // thisPunct is true when the current word ends with Punctuation
                // afterPunct is true when the current word follows a word ending with Punctuation
                boolean thisPunct = false;
                boolean afterPunct = false;

                while( sc.hasNext() ) { //while not end of file is reached
                    String word = sc.next();  //read word by word


                    if(thisPunct) { 
                        afterPunct = true;
                    }

                    thisPunct = false;


                    Pattern midpunct = Pattern.compile("\\p{Alpha}\\p{Punct}\\p{Alpha}"); //find patterns in words with mid punctuation
                    Matcher midmatcher = midpunct.matcher(word);

                    if(midmatcher.find()) { //if there is a word with mid punctuation continue without saving it
                      
                        continue;
                    }


                    //// THIS PART REMOVES THE PUNCTUATION ////

                    Pattern p = Pattern.compile("\\p{Punct}"); //find words with punctuation in the end
                    Matcher m = p.matcher(word);

                    if(m.find()) { 

                        // if a word starts with punctuation
                        if(m.start() == 0) {

                          
                            word = word.substring(1,word.length());
                        }

                        //if a word with punctuation in the end is found save the word without the punctuation
                        if(m.start() == (word.length()-1)) {
                            word = word.substring(0,m.start());
                        }

                        thisPunct = true;
                    }


                    /////////////////Spaces will be trimmed////////////////////
                    word = word.trim();


                    // if a word that follows a word with punctuation in the end is not fully in caps or fully in small letters, print the small letter version
                    if (afterPunct) {
                        if((word != word.toLowerCase()) && (word != word.toUpperCase())){
                            word = word.toLowerCase();

                        }
                        afterPunct = false;
                    }

                    if(!isAlpha(word)) {
                            continue;
                    }
                    if (set.add(word)) {
                        createTree(word, root, tree);
                    }
              
                } 
                sc.close();
            }
           
        }catch(Exception ex) {
            System.out.println("File not found.");
        }    
    }
    
    public static File createUniqueWordText(Set<String> set) throws FileNotFoundException, UnsupportedEncodingException {
        File dictionaryInformation = new File("Dictwords.txt");
        PrintWriter UniqueWordText = new PrintWriter("DictWords.txt", "UTF-8");

        String[] SetString = set.toArray(new String[set.size()]);
        for(int i=0; i< SetString.length; i++) {
           UniqueWordText.println(SetString[i]);
        }
        UniqueWordText.println("DictWords.txt, "+SetString.length+" words");
        UniqueWordText.close();
        return dictionaryInformation;
    }
    
    public static List<String> recursiveSuggest(DictNode currentNode, String word, List<String> suggestions) {
        DictNode nextNode;
       
        for(int j = 0; j < 26; j++) {
            
            if(currentNode.children[j] != null) {
                
                 nextNode = currentNode.children[j];
                
                if(nextNode.isWordEnd) {
                    suggestions.add(word+nextNode.data);
                }
                suggestions = recursiveSuggest(nextNode, word+nextNode.data, suggestions);
                continue;
            }
        }
        return suggestions;
    
    }
    
    
    
    public static void suggest(DictNode root, DictTree Tree, String Input) {
        List<String> suggestions = new ArrayList<String>();
        int n = 0;
        String word = "";
        DictNode currentNode = root;
        
        for(int i = 0; i < Input.length(); i++) {
            
            char currentChar = Input.charAt(i);
            if (isLowerCase(currentChar)) {
                
                n = (int) currentChar - (int) 'a';    
            }
            else{
                n = (int) currentChar - (int) 'A';
            }
            
            if(currentNode.children[n] == null) {
              
                System.out.println("There are no words with that prefix");
                return;
            }
            else {
                currentNode = currentNode.children[n];
                word = word + currentChar;               
            }   
        }
        
        suggestions = recursiveSuggest(currentNode, word, suggestions);
        System.out.println("SUGGESTIONS: "+suggestions);
    }

    
    
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, ClassNotFoundException {
        DictNode root = new DictNode();
        DictTree tree = new DictTree(root);
        Set<String> set = new HashSet<String>();
        String input = "";
        Scanner scanner = new Scanner(System.in);
        File dictionaryInformation = null;
        List <String> sugs = new ArrayList<String>();
        
        while(true) {
            
            
            //System.out.println("Tree size: "+ tree.size+ "\nSet size: " +set.size());
            
            
            System.out.println("----------- MENU -----------"
                + "\n\t1. Load dictionary from binary file (type: load fromFilepath)"
                + "\n\t2. Save dictionary to binary file (type: save toFilepath)"
                + "\n\t3. Populate dictionary from txt file (type: read fromTxtFilePath)"
                + "\n\t4. Suggest word (type: suggest wordPhrase)"
                + "\n\t5. Print dictionary information (type: print)"
                + "\n\t6. Quit (type: quit)");
            
            input = scanner.nextLine();
            String[] inputArray = input.split("\\s+"); 

            try { 
                switch(inputArray[0]) {

                    case ("load"): 
                        
                        tree = deserialize(inputArray[1]);
                        root = tree.root;
                        traverse(tree, root, sugs);
                        break;

                    case ("save"):
                        
                        if (tree.size == 1) {     
                            System.out.println("You need to populate a dictionary first.");
                            break;
                        }
                        //System.out.println("Tree size: "+ tree.size+ "\nSet size: " +set.size());
                        serialize(tree, inputArray[1]);
                        System.out.println("Serialization successfull");
                        System.out.println(tree.size);
                        break;

                    case ("read"):
                        readFiles(inputArray[1], root, tree, set);
                        System.out.println("Dictionary populated successfully");
                        //dictionaryInformation = createUniqueWordText(set);
                        break;

                    case ("suggest"):
                        
                        if(tree.size()==1) {
                            System.out.println("You need to populate a dictionary first.");
                            break;
                        }

                        suggest(root, tree, inputArray[1]);
                        break;

                    case ("print"): 
                        if(set.size()==0) {
                            System.out.println("You need to give a file from which a Dictionary can be created.");
                            break;
                        }
                        
                        createUniqueWordText(set);
                        System.out.println("A text file with the Dictionary words was created.");
                        break;

                    case ("quit"): 
                        if(inputArray.length != 1) {
                            System.out.println("Please choose a valid option.");
                            break;
                        }

                        return;
                
                    default: 
                        System.out.println("Please choose a valid option.");    
                }
                
            }catch(ArrayIndexOutOfBoundsException e) {
                System.out.println("~~~Please choose a valid option.");
            }
            System.out.println("Tree size: "+ tree.size+ "\nSet size: " +set.size());
        }
    }
}
