/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ce325.hw1;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Root;
import static java.lang.Character.isLowerCase;
//import static javafx.scene.input.KeyCode.T;

/**
 *
 * @author JOHN
 */
public class CallTree {
    
    
    public static void main(String[] args) {
        String word1 = "at";
        String word2 = "add";
        String word3 = "attack";
        String word4 = "Addon";
        //String currentPrefix;
       // currentPrefix = "";
		//char currentCharacter;
        int n;
        n = 0;
        
        
        DictNode Root = new DictNode();
        DictNode newLeaf = null;
        DictTree Tree = new DictTree(Root);
        
        DictNode currentNode = Root;
        
		
		// loop for the first word
        for(int i = 0; i < word2.length(); i++) {
            char currentChar = word2.charAt(i);
            System.out.println("Current Letter " +currentChar);
            if (isLowerCase(currentChar)) {
                n = (int) currentChar - (int) 'a';
               // System.out.println(n);
                
            }
            else{
                n = (int) currentChar - (int) 'A';
               // System.out.println(n);
            }
            
            
            //currentCharacter = word2.charAt(i); //ETSI LEITOURGEI I SUBSTRING 
            //System.out.println(currentPrefix);
            
			
			// each for loop that isn't for the first word, has the following check, to see if the leaf (letter/prefix/word) already exists
            if(currentNode.children[n] == null) {
                System.out.println(currentNode.data +" has not child at "+ n+ ". It is now added.");
                newLeaf = Tree.addLeaf(currentChar, currentNode, n);
            
                
                currentNode = newLeaf;
            }
            else {
               System.out.println(currentNode.data +" already has a child at "+ n );
                currentNode = currentNode.children[n];
                
            }
            
            if(i==word1.length() - 1) {
                System.out.println("\n THIS IS A LEAF. The word is "+ word2 +"\n");
                currentNode.isLeaf = true;
            }
                
          
        }
        
        System.out.println("\nRETURN TO ROOT//////////////////////////\n");
        currentNode = Root;
        
        
    }
    
}
