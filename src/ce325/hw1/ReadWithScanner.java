/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ce325.hw1;
import java.util.Scanner;
import java.io.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.util.regex.*;

/**
 *
 * @author JOHN
 */
public class ReadWithScanner {
    
    
    public  static boolean isAlpha(String  word) {
        return word.matches("[a-zA-Z]+");
    }
    
    
    
    public static void main(String []args) {

		try {
			Scanner sc = new Scanner(new File("C:\\Users\\christos\\Desktop\\03.txt")); //open and read file from path
                        PrintWriter writer = new PrintWriter("outputtes2t.txt", "UTF-8");
                        
                        // thisPunct is true when the current word ends with Punctuation
                        // afterPunct is true when the current word follows a word ending with Punctuation
                        boolean thisPunct = false;
                        boolean afterPunct = false;
                        
			while( sc.hasNext() ) { //while not end of file is reached
                            String word = sc.next();  //read word by word
                        
                        
                          
                        
                        
                        
                        
                            if(thisPunct) { 
                                afterPunct = true;
                            }

                            thisPunct = false;


                            Pattern midpunct = Pattern.compile("\\p{Alpha}\\p{Punct}\\p{Alpha}"); //find patterns in words with mid punctuation
                            Matcher midmatcher = midpunct.matcher(word);

                            if(midmatcher.find()) { //if there is a word with mid punctuation continue without saving it
                                System.out.println("SKIPPAROUME WORD:"+word);
                                continue;

                            }


                            //// THIS PART REMOVES THE PUNCTUATION ////

                            Pattern p = Pattern.compile("\\p{Punct}"); //find words with punctuation in the end
                            Matcher m = p.matcher(word);

                            if(m.find()) { 
                               
                                // if a word starts with punctuation
                                if(m.start() == 0) {
                                    
                                    System.out.println("Match stin arxi");
                                    //System.out.println("Xaraktiras sti thesi 0: " + word.charAt(0) + " sti thesi 1: " +word.charAt(1)+ " sti teleutaia thesi: " +word.charAt(word.length()-1));
                                    System.out.println("I leksi itan: "+ word);
                                    word = word.substring(1,word.length());
                                    System.out.println("I leksi twra einai: " +word);
                                }
                               
                                //if a word with punctuation in the end is found save the word without the punctuation
                                if(m.start() == (word.length()-1)) {
                                    word = word.substring(0,m.start());
                                }
                                
                                thisPunct = true;
                                //System.out.println("Word WITHOUT PUNC "+ word);
                            }





                            /////////////////Spaces will be trimmed////////////////////
                            word = word.trim();


                            // if a word that follows a word with punctuation in the end is not fully in caps or fully in small letters, print the small letter version
                            if (afterPunct) {
                                if((word != word.toLowerCase()) && (word != word.toUpperCase())){
                                    word = word.toLowerCase();

                                }
                                afterPunct = false;
                            }

                            if(!isAlpha(word)) {
                                    System.out.println("THE WORD THAT IS SKIPPED IS: "+ word);
                                    continue;
                            }

                            
                          
                           //AUTO KANEI PRINT SE ARXEIO
                             writer.println(word);
                           //AUTO KANEI PRINT STIN KONSOLA      
                           //System.out.println(word);
                        
			} 
                        
                        writer.close();

		} catch(Exception ex) {
			ex.printStackTrace();
		}
	
	}
}
